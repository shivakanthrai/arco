package com.arco.bo.content;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arco.dao.content.ContentDAO;
import com.arco.model.ArcoContent;

@Service
public class ContentBOImpl implements ContentBO {

	@Autowired
	private ContentDAO contentDAO;
	
	@Override
	@Transactional(readOnly=false,rollbackFor={Exception.class})
	public void save(ArcoContent arcoContent) {
		contentDAO.save(arcoContent);
	}

	@Override
	@Transactional(readOnly=true,rollbackFor={Exception.class})
	public ArcoContent getContentById(Long contenId) {
		return contentDAO.getContentById(contenId);
	}

}
