package com.arco.bo.content;

import com.arco.model.ArcoContent;

public interface ContentBO {

	void save(ArcoContent arcoContent); 
	
	ArcoContent getContentById(Long contenId);

}
