package com.arco.bo.user;

import java.util.List;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.arco.exception.ArcoBusinessException;
import com.arco.model.ArcoUser;
import com.arco.model.EmailVerificationStatus;

public interface UserBO {
	 
	ArcoUser creatUser(ArcoUser arcoUser) throws ArcoBusinessException;
	
	ArcoUser updateUser(ArcoUser arcoUser)  throws ArcoBusinessException;

	List<ArcoUser> getUsers();

	ArcoUser login(String username, String password); 
	
	ArcoUser getUserByUsername(String username,boolean includeInactiveUsers);
	
	ArcoUser loadUserByUsername(String username) throws UsernameNotFoundException;

	ArcoUser getUserForSendingEmail();

	boolean verifyEmail(String securityRandomId, String emailRandomId) throws Exception; 
	
	EmailVerificationStatus getEmailVerificationEntry(Long userId);

}
