package com.arco.bo.user;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.arco.dao.user.UserDAO;
import com.arco.exception.ArcoBusinessException;
import com.arco.model.ArcoUser;
import com.arco.model.EmailVerificationStatus;
import com.arco.services.external.ArcoService;

@Service
public class UserBOImpl implements UserBO,UserDetailsService{

	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private ArcoService arcoService;
	
	@Override
	@Transactional(readOnly=false,rollbackFor={Exception.class})
	public ArcoUser creatUser(ArcoUser arcoUser) throws ArcoBusinessException{
		try {
			arcoUser.setEmailSentStatus("PENDING");
			userDAO.save(arcoUser);
			
			EmailVerificationStatus verificationStatus = new EmailVerificationStatus();
			verificationStatus.setUserId(arcoUser.getId());
			verificationStatus.setSecurityRandomId(UUID.randomUUID().toString());
			verificationStatus.setEmailRandomId(UUID.randomUUID().toString());
			verificationStatus.setVerificationStatus("PENDING");
			userDAO.save(verificationStatus);
			
			return arcoUser;
		}catch (Exception e) {
			e.printStackTrace();
			throw new ArcoBusinessException("User could not created");
		}
	}
	
	@Override
	@Transactional(readOnly=false,rollbackFor={Exception.class})
	public ArcoUser updateUser(ArcoUser arcoUser) throws ArcoBusinessException{
		try {
			userDAO.save(arcoUser);
			return arcoUser;
		}catch (Exception e) {
			e.printStackTrace();
			throw new ArcoBusinessException("User could not saved");
		}
	}

	@Override
	public List<ArcoUser> getUsers() {
		return userDAO.getUsers();
	}

	@Override
	@Transactional(readOnly=true)
	public ArcoUser login(String username, String password) {
		ArcoUser user = userDAO.getUserByUsername(username,false);
		
	    if(user == null) {
	    		return null;
	    }else {
	    		if(user.getPassword().equals(password)) {
	    			return user;
	    		}
	    }
		
	    return null;
	}

	@Override
	@Transactional(readOnly=true)
	public ArcoUser getUserByUsername(String username,boolean includeInactiveUsers) {
		ArcoUser user = userDAO.getUserByUsername(username,includeInactiveUsers);
		return user;
	}
	
	@Override
	public ArcoUser loadUserByUsername(String username) throws UsernameNotFoundException {
		
		ArcoUser user = userDAO.getUserByUsername(username,false);
		
		if(user == null) {
			throw new UsernameNotFoundException("User not present");
		}
		return user;
	}

	@Override
	@Transactional(readOnly=true)
	public ArcoUser getUserForSendingEmail() {
		return userDAO.getUserForSendingEmail();
	}

	@Override
	@Transactional(readOnly=false,rollbackFor={Exception.class})
	public boolean verifyEmail(String securityRandomId, String emailRandomId) throws Exception {
		 EmailVerificationStatus emailVerificationStatus =  userDAO.getEmailVerificationEntry(securityRandomId,emailRandomId);
		 
		 if(emailVerificationStatus != null) {
			 ArcoUser arcoUser = userDAO.getUserById(emailVerificationStatus.getUserId());
			 if(arcoUser != null) {
				 arcoUser.setActive(true);
				 userDAO.save(arcoUser);
				 emailVerificationStatus.setVerificationStatus("VERIFIED");
				 arcoService.sendRegistrationSuccess(arcoUser);
				 return true;
			 }else {
				 return false;
			 }
		 }else {
			 return false;
		 }
	}
	
	@Override
	@Transactional(readOnly=true)
	public EmailVerificationStatus getEmailVerificationEntry(Long userId) {
		return userDAO.getEmailVerificationEntry(userId);
	}

}
