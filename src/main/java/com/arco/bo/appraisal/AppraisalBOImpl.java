package com.arco.bo.appraisal;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.arco.dao.location.LocationDAO;
import com.arco.model.AppraisalRequest;
import com.arco.model.ArcoUser;
import com.arco.model.Location;
import com.arco.model.LocationFieldSection;
import com.arco.model.LocationImage;
import com.arco.model.PredefinedValue;
import com.arco.services.external.ArcoService;
import com.arco.services.external.exception.RequestFetchingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class AppraisalBOImpl implements AppraisalBO {
	
	private final String USER_AGENT = "Mozilla/5.0";

	@Autowired
	private LocationDAO locationDAO;
	
	@Value("${cloudinary.key}")
	private String cloudinaryKey;
	
	@Value("${cloudinary.secret}")
	private String cloudinarySecret;
	
	@Value("${cloudinary.searchUrl}")
	private String cloudinarySearchUrl;
	
	@Autowired
	private ArcoService arcoService;
	
	
	@Override
	@Transactional(readOnly=true)
	public List<AppraisalRequest> getPendingAppraisalsForUser(ArcoUser arcoUser) {
		try {
			List<AppraisalRequest> appraisalRequests = arcoService.getPendingAppraisalsForUser(arcoUser);
			
//			AppraisalRequest appraisalRequest = new AppraisalRequest();
//			appraisalRequest.setAddress("someting");;
//			appraisalRequest.setRequestCode("abcd");
//			appraisalRequest.setRequestDescription("sometyhing desc");
//			
//			Location location = new Location();
//			location.setAddress("location address");
//			location.setLocationCode("loc code");
//			location.setLocationName("locataion name");
//			location.setLocationType(43);
//			
//			appraisalRequest.setLocations(Arrays.asList(location));
//			appraisalRequests.add(appraisalRequest);
			
			
			
			return appraisalRequests;
		} catch (RequestFetchingException e) {
			return new ArrayList<>();
		}
	}
	
	
	@Override
	@Transactional(readOnly=true)
	public List<Location> getLocations(int pageNo,int pageSize) {
		return locationDAO.getLocations(pageNo,pageSize);
	}

	@Override
	@Transactional(readOnly=true)
	public List<LocationFieldSection> getLocationFieldSections(String locationCode) {
		try {
			return arcoService.getLocationFieldSections(locationCode);
		} catch (RequestFetchingException e) {
			return new ArrayList<>();
		}
	}

	@Override
	@Transactional(readOnly=true)
	public List<PredefinedValue> getLocationPredefinedFieldValues(String fieldCode) {
		try {
			return arcoService.getLocationPredefinedFieldValues(fieldCode);
		} catch (RequestFetchingException e) {
			return new ArrayList<>();
		}
	}

	@Override
	public LocationImage[] getLocationImages(Long locationId) {
		
		try {
	        
			URL obj = new URL(cloudinarySearchUrl+"?expression=tags=location_"+locationId);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			
			String userpass = cloudinaryKey + ":" + cloudinarySecret;
	        String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
	        con.setRequestProperty("Authorization", basicAuth);
	        
			// optional default is GET
			con.setRequestMethod("GET");

			//add request header
			con.setRequestProperty("User-Agent", USER_AGENT);

			int responseCode = con.getResponseCode();
			
			if(responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader in = new BufferedReader(
				        new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				ObjectMapper mapper = new ObjectMapper();
				
				JSONObject jsonObject = new JSONObject(response.toString());
				
				JSONArray resourcesJsonArray = jsonObject.getJSONArray("resources");
				LocationImage[] images = mapper.readValue(resourcesJsonArray.toString(), LocationImage[].class);
		        System.out.println("resources = " + Arrays.toString(images));
				return images;
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	@Transactional(readOnly=false)
	public void save(String requestCode, String locationCode, LocationFieldSection[] sections) throws Exception {
		try {
			arcoService.saveLocationDetails(requestCode,locationCode,sections);
		} catch (RequestFetchingException e) {
			throw new Exception();
		}
	}

	@Override
	@Transactional(readOnly=true)
	public List<LocationImage> getLocationImagesFromDatabase(Long locationId) {
		return locationDAO.getLocationImagesFromDatabase(locationId);
	}

	@Override
	@Transactional(readOnly=false)
	public void save(LocationImage locationImage) {
		locationDAO.save(locationImage);
	}

	@Override
	@Transactional(readOnly=true)
	public LocationImage getLocationImage(String uuid) {
		LocationImage locationImage = locationDAO.getLocationImage(uuid);
		return locationImage;
	}

	@Override
	@Transactional(readOnly=false)
	public void delete(Long imageId) {
		locationDAO.delete(imageId);
		
	}


	@Override
	public void uploadLocationPicture(String locationCode, String encodedImage, String imageType) throws Exception {
		try {
			arcoService.uploadLocationPicture(locationCode,encodedImage,imageType);
		} catch (RequestFetchingException e) {
			throw new Exception();
		}
		
	}


	@Override
	public void saveRequestLocation(String requestCode, String latitude, String longitude) throws Exception {
		try {
			arcoService.saveRequestLocation(requestCode,latitude,longitude);
		} catch (RequestFetchingException e) {
			throw new Exception();
		}
		
	}

}
