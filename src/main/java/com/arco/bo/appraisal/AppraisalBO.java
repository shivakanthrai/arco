package com.arco.bo.appraisal;

import java.util.List;

import com.arco.model.AppraisalRequest;
import com.arco.model.ArcoUser;
import com.arco.model.Location;
import com.arco.model.LocationFieldSection;
import com.arco.model.LocationImage;
import com.arco.model.PredefinedValue;

public interface AppraisalBO {
	
	List<AppraisalRequest> getPendingAppraisalsForUser(ArcoUser arcoUser);  
	
	List<Location> getLocations(int pageNo, int pageSize);

	List<LocationFieldSection> getLocationFieldSections(String locationCode);

	List<PredefinedValue> getLocationPredefinedFieldValues(String fieldCode);

	LocationImage[] getLocationImages(Long locationId);

	void save(String requestCode, String locationCode, LocationFieldSection[] sections) throws Exception;

	List<LocationImage> getLocationImagesFromDatabase(Long locationId);

	void save(LocationImage locationImage);

	LocationImage getLocationImage(String uuid);

	void delete(Long imageId);

	void uploadLocationPicture(String locationCode, String encodedImage, String imageType) throws Exception;

	void saveRequestLocation(String requestCode, String latitude, String longitude) throws Exception; 

	

}
