package com.arco.bo.audit;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arco.dao.audit.AuditDAO;
import com.arco.exception.ArcoBusinessException;
import com.arco.model.ArcoUser;
import com.arco.model.LoginAudit;

@Service
public class AuditBOImpl implements AuditBO {
	
	
	@Autowired
	private AuditDAO auditDAO;

	@Override
	@Transactional(readOnly=false,rollbackFor={ArcoBusinessException.class})
	public void saveLoginAudit(ArcoUser user, HttpServletRequest request) throws ArcoBusinessException{
		
		try{
			LoginAudit loginAudit = new LoginAudit();
			loginAudit.setUserId(user.getId());
			loginAudit.setDeviceId(request.getRemoteUser());
			loginAudit.setRemodeAddress(request.getRemoteAddr());
			loginAudit.setRemoteHost(request.getRemoteHost());
			loginAudit.setLoggedInTime(Calendar.getInstance().getTime());
			auditDAO.save(loginAudit);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
