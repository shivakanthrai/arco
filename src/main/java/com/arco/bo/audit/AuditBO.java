package com.arco.bo.audit;

import javax.servlet.http.HttpServletRequest;

import com.arco.exception.ArcoBusinessException;
import com.arco.model.ArcoUser;

public interface AuditBO {
	void saveLoginAudit(ArcoUser user,HttpServletRequest request)  throws ArcoBusinessException;
}
