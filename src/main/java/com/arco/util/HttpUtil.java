package com.arco.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

public class HttpUtil {

	public static String post(String endpoint, String body, Map<String, String> headers) throws HttpClientException {

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(endpoint);

		// add headers
		if (headers != null) {
			for (String headerParamName : headers.keySet()) {
				post.setHeader(headerParamName, headers.get(headerParamName));
			}
		}

		StringEntity requestEntity = new StringEntity(body, ContentType.APPLICATION_JSON);
		post.setEntity(requestEntity);

		try {
			HttpResponse response = client.execute(post);

			int responseCode = response.getStatusLine().getStatusCode();
			System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

			if (responseCode != 200) {
				throw new HttpClientException(responseCode, response.getStatusLine().getReasonPhrase());
			}

			

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(),StandardCharsets.UTF_8));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			return result.toString();
		} catch (Exception e) {
			e.printStackTrace();
			throw new HttpClientException(500, "Interal Server Error");
		}

	}
	
	public static String get(String endpoint,Map<String, String> headers,Map<String, String> params) throws HttpClientException {

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = null;
		try {
			URIBuilder builder = new URIBuilder(endpoint);
			
			if (params != null) {
				List<NameValuePair> nameValuePairs = new ArrayList<>();
				for (String paramName : params.keySet()) {
					NameValuePair nameValuePair = new BasicNameValuePair(paramName, params.get(paramName));
					nameValuePairs.add(nameValuePair);
				}
				
				builder.addParameters(nameValuePairs);
			}
			
			request = new HttpGet(builder.build());

			// add headers
			if (headers != null) {
				for (String headerParamName : headers.keySet()) {
					request.setHeader(headerParamName, headers.get(headerParamName));
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			throw new HttpClientException(400, "Bad Rwquest");
		}
		

		try {
			HttpResponse response = client.execute(request);

			int responseCode = response.getStatusLine().getStatusCode();

			if (responseCode != 200) {
				throw new HttpClientException(responseCode, response.getStatusLine().getReasonPhrase());
			}

			System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(),StandardCharsets.UTF_8));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			return result.toString();
		} catch (Exception e) {
			e.printStackTrace();
			throw new HttpClientException(500, "Interal Server Error");
		}

	}

	
}
