package com.arco.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

public class ArcoAuthenticationManager implements AuthenticationManager {

	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private PasswordEncoder encoder;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getPrincipal() + "";
	    String password = authentication.getCredentials() + "";

	    UserDetails user = userDetailsService.loadUserByUsername(username);
	    if (user == null) {
	        throw new BadCredentialsException("1000");
	    }
	    if (!user.isEnabled()) {
	        throw new DisabledException("1001");
	    }
	    if (!encoder.matches(password, user.getPassword())) {
	        throw new BadCredentialsException("1000");
	    }
	    
	    List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
	    authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
	    
	    return new UsernamePasswordAuthenticationToken(user, password,authorities);
	}

}
