package com.arco.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
public class AppConfig extends WebMvcConfigurerAdapter{
	@Value("${spring.datasource.url}")
    private String datasourceUrl;
    
    @Value("${spring.datasource.driver-class-name}")
    private String dbDriverClassName;
    
    @Value("${spring.datasource.username}")
    private String dbUsername;
    
    @Value("${spring.datasource.password}")
    private String dbPassword;
    
    @Value("${spring.jpa.properties.hibernate.entitymanager.packagesToScan}")
    private String packagesToScan;
    
    @Value("${spring.jpa.properties.hibernate.dialect}")
    private String hibernateDialect;
    
    @Value("${spring.jpa.properties.hibernate.show-sql}")
    private String showSql;
    
    @Value("${spring.jpa.hibernate.ddl-auto}")
    private String ddlAuto;
    
    
    @Bean
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        
        dataSource.setDriverClassName(dbDriverClassName);
        dataSource.setUrl(datasourceUrl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);
        
        return dataSource;
    }
    
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
      LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
      sessionFactoryBean.setDataSource(dataSource());
      sessionFactoryBean.setPackagesToScan(packagesToScan);
      Properties hibernateProperties = new Properties();
      hibernateProperties.put("hibernate.dialect", hibernateDialect);
      hibernateProperties.put("hibernate.show_sql", showSql);
      hibernateProperties.put("hibernate.id.new_generator_mappings","false");
      sessionFactoryBean.setHibernateProperties(hibernateProperties);
      
      return sessionFactoryBean;
    }

  

	@Bean
    public HibernateTransactionManager transactionManager() {
      HibernateTransactionManager transactionManager = 
          new HibernateTransactionManager();
      transactionManager.setSessionFactory(sessionFactory().getObject());
      return transactionManager;
    }
    
    @Bean
    public TokenStore tokenStore() {
        return new JdbcTokenStore(dataSource());
    }
    
    
   
}
