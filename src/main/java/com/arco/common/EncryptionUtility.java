package com.arco.common;

import java.io.InputStream;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.sun.mail.util.BASE64DecoderStream;
import com.sun.mail.util.BASE64EncoderStream;

@Service("encryptionUtility")
public class EncryptionUtility {

	private Cipher ecipher;
	private SecretKey ky;
	
	public EncryptionUtility() throws Exception{
		Resource resource = new ClassPathResource("keyfile.key");
	    InputStream fis = resource.getInputStream();
	    int kl = fis.available();
	    byte[] kb = new byte[kl];
	    fis.read(kb);
	    fis.close();
	    KeySpec ks = null;
	    this.ky = null;
	    SecretKeyFactory kf = null;
	    try {
			ks = new DESKeySpec(kb);
			kf = SecretKeyFactory.getInstance("DES");
		    this.ky = kf.generateSecret(ks);
		    this.ecipher = Cipher.getInstance("DES");
        } catch (Exception e) {
			throw e;
		}
	}
	
	public synchronized String encryptString(String stringToEncrypt){
		try{
			ecipher.init(Cipher.ENCRYPT_MODE, ky);
			byte[] utf8 = stringToEncrypt.trim().getBytes("UTF8");
			byte[] enc = ecipher.doFinal(utf8);
			enc = BASE64EncoderStream.encode(enc);
			return new String(enc);
		}catch(Exception e){
			return "";
		}
	}
	
	public synchronized String decryptString(String stringToDecrypt){
		try{
			ecipher.init(Cipher.DECRYPT_MODE, ky);
			byte[] utf8 = stringToDecrypt.trim().getBytes("UTF8");
			byte[] enc = BASE64DecoderStream.decode(utf8);
			enc = ecipher.doFinal(enc);
			return new String(enc);
		}catch(Exception e){
			return "";
		}
	}
}
