package com.arco.dao.audit;

import org.springframework.stereotype.Repository;

import com.arco.dao.CustomHibernateDaoSupport;

@Repository
public class AuditDAOImpl extends CustomHibernateDaoSupport implements AuditDAO {

	@Override
	public void save(Object entity) {
		getHibernateTemplate().save(entity);

	}

}
