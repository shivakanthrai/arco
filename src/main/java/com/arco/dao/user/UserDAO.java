package com.arco.dao.user;

import java.util.List;

import com.arco.model.ArcoUser;
import com.arco.model.EmailVerificationStatus;

public interface UserDAO {
	void save(Object object);

	List<ArcoUser> getUsers();

	ArcoUser getUserByUsername(String username,boolean includeInactiveUsers);

	void generateUserRegistrationEvent(Long userId);

	ArcoUser getUserForSendingEmail();

	EmailVerificationStatus getEmailVerificationEntry(String securityRandomId, String emailRandomId);

	ArcoUser getUserById(Long userId);

	EmailVerificationStatus getEmailVerificationEntry(Long userId); 
}
