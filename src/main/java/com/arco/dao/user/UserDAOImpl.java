package com.arco.dao.user;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import com.arco.dao.CustomHibernateDaoSupport;
import com.arco.model.ArcoUser;
import com.arco.model.EmailVerificationStatus;
import com.arco.model.UserRegistrationEvent;

@Repository
public class UserDAOImpl extends CustomHibernateDaoSupport implements UserDAO {

	public void save(Object object) {
		getHibernateTemplate().saveOrUpdate(object);
	}

	@Override
	public List<ArcoUser> getUsers() {
		return (List<ArcoUser>)getHibernateTemplate().find("from ArcoUser");
	}

	@Override
	public ArcoUser getUserByUsername(String username,boolean includeInactiveUsers) {
		List<ArcoUser> arcoUsers = null;
		if(!includeInactiveUsers) {
			arcoUsers = (List<ArcoUser>)getHibernateTemplate().find("from ArcoUser where username = ?0 and isActive = ?1",username,true);
		}else {
			arcoUsers = (List<ArcoUser>)getHibernateTemplate().find("from ArcoUser where username = ?0",username);
		}
		
		
		if(arcoUsers !=null && arcoUsers.size() > 0) {
			return arcoUsers.get(0);
		}
		
		return null;
	}

	@Override
	public ArcoUser getUserById(Long userId) {
		List<ArcoUser> arcoUsers = (List<ArcoUser>)getHibernateTemplate().find("from ArcoUser where id = ?0 and isActive = ?1",userId,false);
		
		if(arcoUsers.size() > 0) {
			return arcoUsers.get(0);
		}
		
		return null;	
	}
	
	@Override
	public void generateUserRegistrationEvent(Long userId) {
		Date now  = Calendar.getInstance().getTime();
		UserRegistrationEvent event = new UserRegistrationEvent();
		event.setUserId(userId);
		event.setNotificationStatus(ArcoUser.EVENT_USER_REGISTERED);
		event.setCreationDate(now);
		getHibernateTemplate().save(event);
	}

	@Override
	public ArcoUser getUserForSendingEmail() {
		Query<ArcoUser> query = getSessionFactory().getCurrentSession().createQuery("from ArcoUser where emailSentStatus = :emailSentStatus and isActive = false");
		query.setParameter("emailSentStatus", "PENDING");
		query.setFetchSize(1);
		List<ArcoUser> arcoUsers = query.getResultList();
		
		if(arcoUsers != null && arcoUsers.size() >0) {
			return arcoUsers.get(0);
		}
		return null;
	}

	@Override
	public EmailVerificationStatus getEmailVerificationEntry(String securityRandomId, String emailRandomId) {
		List<EmailVerificationStatus> emailVerificationStatus = (List<EmailVerificationStatus>)getHibernateTemplate().find("from EmailVerificationStatus where securityRandomId = ?0 and emailRandomId = ?1",securityRandomId,emailRandomId);
		
		if(emailVerificationStatus.size() > 0) {
			return emailVerificationStatus.get(0);
		}
		
		return null;
	}
	
	
	@Override
	public EmailVerificationStatus getEmailVerificationEntry(Long userId) {
		List<EmailVerificationStatus> emailVerificationStatus = (List<EmailVerificationStatus>)getHibernateTemplate().find("from EmailVerificationStatus where userId = ?0",userId);
		
		if(emailVerificationStatus.size() > 0) {
			return emailVerificationStatus.get(0);
		}
		
		return null;
	}

	

}
