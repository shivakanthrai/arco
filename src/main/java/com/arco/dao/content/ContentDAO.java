package com.arco.dao.content;

import com.arco.model.ArcoContent;

public interface ContentDAO {

	void save(ArcoContent arcoContent);
	ArcoContent getContentById(Long contenId);
	
}
