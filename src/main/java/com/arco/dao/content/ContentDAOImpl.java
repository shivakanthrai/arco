package com.arco.dao.content;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.arco.dao.CustomHibernateDaoSupport;
import com.arco.model.ArcoContent;

@Repository
public class ContentDAOImpl extends CustomHibernateDaoSupport implements ContentDAO {

	@Override
	public void save(ArcoContent arcoContent) {
		getHibernateTemplate().save(arcoContent);
	}

	@Override
	public ArcoContent getContentById(Long contentId) {
		List<ArcoContent> arcoContents = (List<ArcoContent>) getHibernateTemplate().find("from ArcoContent where id=?0", contentId);
		
		if(arcoContents != null && arcoContents.size() > 0) {
			return arcoContents.get(0);
		}else {
			return null;
		}
		
	}
	
}
