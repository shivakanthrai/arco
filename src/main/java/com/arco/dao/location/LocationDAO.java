package com.arco.dao.location;

import java.util.List;
import com.arco.model.Location;
import com.arco.model.LocationImage;
import com.arco.model.PredefinedValue;

public interface LocationDAO {
	List<Location> getLocations(int pageNo, int pageSize);

	Location getLocation(Long locationId);

	List<LocationImage> getLocationImagesFromDatabase(Long locationId);

	void save(LocationImage locationImage);

	LocationImage getLocationImage(String uuid);

	void delete(Long imageId); 
}
