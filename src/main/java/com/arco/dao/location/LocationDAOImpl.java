package com.arco.dao.location;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.arco.dao.CustomHibernateDaoSupport;
import com.arco.model.Location;
import com.arco.model.LocationImage;
import com.arco.model.PredefinedValue;

@Repository
public class LocationDAOImpl extends CustomHibernateDaoSupport implements LocationDAO {
	
	private static final Integer DEFAULT_PAGE_NO = 1;
	private static final Integer DEFAULT_PAGE_SIZE = 20;

	@Override
	public List<Location> getLocations(int pageNo, int pageSize) {
		
		if(pageNo < DEFAULT_PAGE_NO) {
			pageNo = DEFAULT_PAGE_NO;
		}
		
		if(pageSize < 1) {
			pageSize = DEFAULT_PAGE_SIZE;
		}
		
		Session session = getSessionFactory().getCurrentSession();
		Criteria criteria = session.createCriteria(Location.class);
		criteria.setFirstResult(pageNo-1);
		criteria.setMaxResults(pageSize);
		List<Location> locations = criteria.list();
		return locations;
	}

	@Override
	public Location getLocation(Long locationId) {
		
		try {
			if(locationId != null) {
				Session session = getSessionFactory().getCurrentSession();
				Criteria criteria = session.createCriteria(Location.class);
				criteria.add(Restrictions.eq("id",locationId));
				return (Location) criteria.uniqueResult();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
	}

	

	
	@Override
	public List<LocationImage> getLocationImagesFromDatabase(Long locationId) {
		try {
			if(locationId != null) {
				Session session = getSessionFactory().getCurrentSession();
				Criteria criteria = session.createCriteria(LocationImage.class);
				criteria.add(Restrictions.eq("locationId",locationId));
				criteria.addOrder(Order.desc("uploadedOn"));
				List<LocationImage> images =  criteria.list();
				
				for (LocationImage locationImage : images) {
					locationImage.setData(null);
				}
				return images;
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ArrayList<LocationImage>();
	}

	@Override
	public void save(LocationImage locationImage) {
		getSessionFactory().getCurrentSession().saveOrUpdate(locationImage);
	}

	@Override
	public LocationImage getLocationImage(String uuid) {
		List<LocationImage> locationImages = (List<LocationImage>)getHibernateTemplate().find("from LocationImage where url = ?0",uuid);
		
		if(locationImages.size() > 0) {
			return locationImages.get(0);
		}
		
		return null;
	}
	
	
	private LocationImage getLocationImage(Long imageId) {
		List<LocationImage> locationImages = (List<LocationImage>)getHibernateTemplate().find("from LocationImage where id = ?0",imageId);
		
		if(locationImages.size() > 0) {
			return locationImages.get(0);
		}
		
		return null;
	}

	@Override
	public void delete(Long imageId) {
        List<LocationImage> locationImages = (List<LocationImage>)getHibernateTemplate().find("from LocationImage where id = ?0",imageId);
		
		if(locationImages.size() > 0) {
			getHibernateTemplate().delete(locationImages.get(0));
		}
		
	}

}
