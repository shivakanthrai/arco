package com.arco.services;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.support.HttpRequestWrapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.arco.bo.audit.AuditBO;
import com.arco.bo.content.ContentBO;
import com.arco.bo.user.UserBO;
import com.arco.common.EncryptionUtility;
import com.arco.model.ArcoContent;
import com.arco.model.ArcoUser;
import com.arco.model.LocationImage;
import com.arco.model.RequestResponse;
import com.arco.services.external.ArcoService;
import com.arco.services.external.model.UserCreationApiResponse;
import com.google.gson.Gson;


@RestController
@RequestMapping("/user")
public class UserManagementService {
	
	@Autowired
	private UserBO userBo;
	
	@Autowired
	private AuditBO auditBO;
	
	@Autowired
	private ContentBO contentBO;
	
	@Autowired
	private ArcoService arcoService;
	
	@Value("${arco.services.url}")
	private String arcoServiceUrl;
	
		
	@Autowired
	@Lazy
	private PasswordEncoder passwordEncoder;
 
	
	@PostMapping("/createuser")
	public RequestResponse createUser(@RequestBody ArcoUser arcoUser) {
		RequestResponse response = new RequestResponse();
		
		try {
			ArcoUser securityUser = (ArcoUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			if(securityUser.isAdmin()) {
				arcoUser.setPassword(passwordEncoder.encode(arcoUser.getPassword()));
				arcoUser.setCreationDate(Calendar.getInstance().getTime());
				userBo.creatUser(arcoUser);
				Map<String,Object> model = new HashMap<>();
				model.put("user", arcoUser);
				response.setModel(model);
			}else {
				response.setHasError(true);
				response.setMessage("User doesn't have access");
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setHasError(true);
			response.setMessage("User could not be added");
		}

		return response;
	}
	
	@PostMapping("/registeruser")
	public RequestResponse registerUser(@RequestBody ArcoUser arcoUser) {
		RequestResponse response = new RequestResponse();
		try {
			
			ArcoUser user = userBo.getUserByUsername(arcoUser.getEmailId(),true);
			
			if(user != null) {
				response.setHasError(true);
				response.setMessage("USER ALREADY REGISTERED");
			}else {
				UserCreationApiResponse apiResponse = arcoService.createUser(arcoUser.getEmailId());
				
				if(apiResponse == null || apiResponse.getUserCode() == null) {
					throw new Exception();
				}else {
					arcoUser.setUserCode(apiResponse.getUserCode());
				}
				
				if(StringUtils.isEmpty(arcoUser.getFirstName())
				  || StringUtils.isEmpty(arcoUser.getLastName())
				  || StringUtils.isEmpty(arcoUser.getPassword())
				  || StringUtils.isEmpty(arcoUser.getEmailId())) {
					response.setHasError(true);
					response.setMessage("INVALID INPUT");
				}else {
					String actualPassword = arcoUser.getPassword();
					arcoUser.setPassword(passwordEncoder.encode(arcoUser.getPassword()));
					arcoUser.setUsername(arcoUser.getEmailId());
					arcoUser.setActive(false);
					arcoUser.setAdmin(false);
					userBo.creatUser(arcoUser);
					arcoUser.setPassword(actualPassword);
					Map<String,Object> model = new HashMap<>();
					model.put("user", arcoUser);
					response.setMessage("USER REGISTERED");
					response.setModel(model);
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setHasError(true);
			response.setMessage("User could not be added");
		}

		return response;
	}
	
	@PostMapping("/uploaddocument")
	public Map<String,Long> uploadDocument(@RequestParam("file") MultipartFile file,
			@RequestParam("fileName")String fileName,
			@RequestParam("name") String name) {
		HashMap<String, Long> model = new HashMap<String,Long>();
		try {
			
			ArcoContent arcoContent = new ArcoContent();
			arcoContent.setFilename(fileName);
			arcoContent.setName(name);
			arcoContent.setData(file.getBytes());
			arcoContent.setUploadedOn(Calendar.getInstance().getTime());
			arcoContent.setResourceIdentifier(UUID.randomUUID().toString());
			String extension = FilenameUtils.getExtension(file.getOriginalFilename());
			arcoContent.setExtenstion(extension);
			contentBO.save(arcoContent);
			
			
			model.put("contentId", arcoContent.getId());
			System.out.println(model);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return model;
	}
	
	@GetMapping("/getusers")
	public RequestResponse getUsers() {
		RequestResponse response = new RequestResponse();
		
		try {
			List<ArcoUser> arcoUsers = userBo.getUsers();
			Map<String,Object> model = new HashMap<>();
			model.put("users", arcoUsers);
			response.setModel(model);
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setHasError(true);
			response.setMessage("Users could not be fetched");
		}

		return response;
	}
	
	@GetMapping("/getloggedinuser")
	public RequestResponse getLoggedInUser(HttpServletRequest request) {
		RequestResponse response = new RequestResponse();
		
		try {
			ArcoUser securityUser = (ArcoUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			ArcoUser arcoUser = userBo.loadUserByUsername(securityUser.getUsername());
			auditBO.saveLoginAudit(arcoUser, request);
			Map<String,Object> model = new HashMap<>();
			model.put("user", arcoUser);
			model.put("arcoServiceUrl",arcoServiceUrl);
			response.setModel(model);
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setHasError(true);
			response.setMessage("User could not be fetched");
		}

		return response;
	}
	
	@GetMapping("/testtoken")
	public RequestResponse testToken(HttpServletRequest request) {
		RequestResponse response = new RequestResponse();
		
		try {
			
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setHasError(true);
			response.setMessage("Token invalid fetched");
		}

		return response;
	}
	
}
