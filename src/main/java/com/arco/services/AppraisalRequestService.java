package com.arco.services;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.arco.bo.appraisal.AppraisalBO;
import com.arco.model.AppraisalRequest;
import com.arco.model.ArcoUser;
import com.arco.model.Location;
import com.arco.model.LocationFieldSection;
import com.arco.model.LocationImage;
import com.arco.model.PredefinedValue;
import com.arco.model.RequestResponse;
import com.arco.util.ImageUtil;
import com.arco.util.ImageUtil.ImageInformation;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/appraisal")
public class AppraisalRequestService {

	@Autowired
	private AppraisalBO appraisalBO;
	
	@Value("${arco.services.photo.maxwidth}")
	private int imageMaxDimensionLength; 
	

	@GetMapping("/getpendingappraisalsforuser")
	public List<AppraisalRequest> getPendingAppraisalsForUser() {
		List<AppraisalRequest> appraisalRequests = new ArrayList<AppraisalRequest>();

		try {
			ArcoUser securityUser = (ArcoUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			appraisalRequests = appraisalBO.getPendingAppraisalsForUser(securityUser);

		} catch (Exception e) {
			e.printStackTrace();

		}

		return appraisalRequests;
	}

	@GetMapping("/locations")
	public List<Location> getLocations(@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
			@RequestParam(name = "pageSize", required = false, defaultValue = "0") Integer pageSize) {
		List<Location> locations = new ArrayList<Location>();

		try {

			locations = appraisalBO.getLocations(pageNo, pageSize);

		} catch (Exception e) {
			e.printStackTrace();

		}

		return locations;
	}

	@GetMapping("/locationdetails/{locationCode}")
	public List<LocationFieldSection> getLocationFieldSections(
			@PathVariable(name = "locationCode") String locationCode) {
		List<LocationFieldSection> locationFieldSections = new ArrayList<LocationFieldSection>();

		try {

			locationFieldSections = appraisalBO.getLocationFieldSections(locationCode);

		} catch (Exception e) {
			e.printStackTrace();

		}

		return locationFieldSections;
	}

	@GetMapping("/getfieldvalues/{fieldCode}")
	public List<PredefinedValue> getLocationFieldValues(@PathVariable(name = "fieldCode") String fieldCode) {
		List<PredefinedValue> locationAttributeValues = new ArrayList<PredefinedValue>();

		try {

			locationAttributeValues = appraisalBO.getLocationPredefinedFieldValues(fieldCode);

		} catch (Exception e) {
			e.printStackTrace();

		}

		return locationAttributeValues;
	}

	@GetMapping("/locationimages/{locationId}")
	public LocationImage[] getLocationImages(@PathVariable(name = "locationId") Long locationId) {
		LocationImage[] locationImages = null;

		try {
			locationImages = appraisalBO.getLocationImages(locationId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return locationImages;
	}

	@PostMapping("/uploadlocationimage")
	public LocationImage[] uploadImage(@RequestParam("file") MultipartFile file,
			@RequestParam("locationCode") String locationCode,
			@RequestParam("imageType") String imageType) {
		LocationImage[] locationImages = null;
		
		try {
			Image img = null;
			BufferedImage tempPNG = null;
			BufferedImage tempJPG = null;
			File newFileJPG = null;
			
			img = ImageIO.read(file.getInputStream());
			
			int finalWidth = 0;
			int finalHeight = 0;
			double finalRotation = 0;
			
			int imageWidth = img.getWidth(null);
			int imageHeight = img.getHeight(null);
			
			double aspectRatio = (double) imageWidth/(double) imageHeight;
			
			
		    if(imageWidth < imageHeight) {
		    	      if(imageHeight > imageMaxDimensionLength) {
		    	    	      finalHeight = imageMaxDimensionLength;
		    	      }else {
		    	    	  	  finalHeight = imageHeight;
		    	      }
		    	   
		    	      tempJPG = resizeImage(img, (int) (aspectRatio * finalHeight), finalHeight);
		    	      
		    	      ImageInformation imageInformation = ImageUtil.getImageInformation(file.getInputStream());
					  
					  if(imageInformation.orientation == 6 || imageInformation.orientation == 7) {
						  finalRotation = Math.toRadians(90);
					  }else if(imageInformation.orientation == 8 || imageInformation.orientation == 5) {
						  finalRotation = Math.toRadians(-90);
					  }else if(imageInformation.orientation == 3){
						  finalRotation = Math.toRadians(180);
					  }
		    	      
		    }else {
				  if(imageWidth > imageMaxDimensionLength) {
					  finalWidth = imageMaxDimensionLength;
		  	      }else {
		  	    	  	  finalWidth = imageWidth;
		  	      }
  	   
				  tempJPG = resizeImage(img, finalWidth,(int) (finalWidth/aspectRatio));

				  ImageInformation imageInformation = ImageUtil.getImageInformation(file.getInputStream());
				  
				  if(imageInformation.orientation == 6 || imageInformation.orientation == 7) {
					  finalRotation = Math.toRadians(90);
				  }else if(imageInformation.orientation == 8 || imageInformation.orientation == 5) {
					  finalRotation = Math.toRadians(-90);
				  }else if(imageInformation.orientation == 3){
					  finalRotation = Math.toRadians(180);
				  }
				  
		    }
		    
		    
		    BufferedImage rotatedImage = rotateImage(tempJPG, finalRotation);
		    
		    final ByteArrayOutputStream os = new ByteArrayOutputStream();
	        ImageIO.write(rotatedImage, "jpg", os);
	        String encodedImage = Base64.getEncoder().encodeToString(os.toByteArray());
	        
	        appraisalBO.uploadLocationPicture(locationCode,encodedImage,imageType);
			

		} catch (Exception e) {
			e.printStackTrace();
		}

		return locationImages;
	}

	@GetMapping("/locationimagesfromdb/{locationId}")
	public List<LocationImage> getLocationImagesFromDb(@PathVariable(name = "locationId") Long locationId) {
		List<LocationImage> locationImages = null;

		try {
			locationImages = appraisalBO.getLocationImagesFromDatabase(locationId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return locationImages;
	}

	@GetMapping("/locationimage/{uuid}")
	public HttpEntity<byte[]> getLocationImageFromDb(@PathVariable(name = "uuid") String uuid) {
		LocationImage locationImage = null;

		try {
			locationImage = appraisalBO.getLocationImage(uuid);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (locationImage != null) {
			HttpHeaders headers = new HttpHeaders();
			if (locationImage.getFileType().equalsIgnoreCase("JPEG")) {
				headers.setContentType(MediaType.IMAGE_JPEG);
			} else {
				headers.setContentType(MediaType.IMAGE_PNG);
			}

			headers.setContentLength(locationImage.getData().length);

			return new HttpEntity(locationImage.getData(), headers);
		} else {
			return null;
		}
	}

	@PostMapping("/save")
	public List<LocationFieldSection> saveLocation(@RequestBody String jsonBody) throws Exception {
		try {
			ObjectMapper mapper = new ObjectMapper();
			JSONObject jsonObject = new JSONObject(jsonBody);

			String locationCode = jsonObject.getString("locationCode");
			String requestCode = jsonObject.getString("requestCode");
			JSONArray sectionsJsonArray = jsonObject.getJSONArray("sections");
			LocationFieldSection[] sections = mapper.readValue(sectionsJsonArray.toString(),
					LocationFieldSection[].class);
			appraisalBO.save(requestCode, locationCode, sections);
			return appraisalBO.getLocationFieldSections(locationCode);
		} catch (Exception e) {
			throw e;
		}

	}
	
	@PostMapping("/saverequestlocation")
	public boolean saveRequestLocation(@RequestBody String jsonBody) throws Exception {
		try {
			ObjectMapper mapper = new ObjectMapper();
			JSONObject jsonObject = new JSONObject(jsonBody);

			String requestCode = jsonObject.getString("requestCode");
			String latitude = jsonObject.getString("latitude");
			String longitude = jsonObject.getString("longitude");
			appraisalBO.saveRequestLocation(requestCode, latitude, longitude);
			return true;
		} catch (Exception e) {
			throw e;
		}

	}
	

	@PostMapping("/delete/{imageId}")
	public RequestResponse deleteLocationImage(@PathVariable(name = "imageId") Long imageId) throws Exception {
		RequestResponse response = new RequestResponse();
		try {
			appraisalBO.delete(imageId);
		} catch (Exception e) {
			e.printStackTrace();
			response.setHasError(true);
		}
		return response;
	}

	/**
	 * This function resize the image file and returns the BufferedImage object that
	 * can be saved to file system.
	 */
	public BufferedImage resizeImage(final Image image, int width, int height) {
		final BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		final Graphics2D graphics2D = bufferedImage.createGraphics();
		graphics2D.setComposite(AlphaComposite.Src);
		// below three lines are for RenderingHints for better image quality at cost of
		// higher processing time
		graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		graphics2D.drawImage(image, 0, 0, width, height, null);
		graphics2D.dispose();
		return bufferedImage;
	}
	
	public BufferedImage rotateImage(BufferedImage image,double rads) {
		final double sin = Math.abs(Math.sin(rads));
		final double cos = Math.abs(Math.cos(rads));
		final int w = (int) Math.floor(image.getWidth() * cos + image.getHeight() * sin);
		final int h = (int) Math.floor(image.getHeight() * cos + image.getWidth() * sin);
		final BufferedImage rotatedImage = new BufferedImage(w, h, image.getType());
		final AffineTransform at = new AffineTransform();
		at.translate(w / 2, h / 2);
		at.rotate(rads,0, 0);
		at.translate(-image.getWidth() / 2, -image.getHeight() / 2);
		final AffineTransformOp rotateOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		rotateOp.filter(image,rotatedImage);
		
		return rotatedImage;
	}
}
