package com.arco.services.external;

import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.arco.dao.content.ContentDAO;
import com.arco.model.AppraisalRequest;
import com.arco.model.ArcoContent;
import com.arco.model.ArcoUser;
import com.arco.model.LocationField;
import com.arco.model.LocationFieldSection;
import com.arco.model.PredefinedValue;
import com.arco.services.external.exception.RequestFetchingException;
import com.arco.services.external.exception.UserExistsException;
import com.arco.services.external.exception.UserNotCreatedException;
import com.arco.services.external.model.AppraisalListingApiResponse;
import com.arco.services.external.model.ArcoApiResponse;
import com.arco.services.external.model.FieldValueListApiResponse;
import com.arco.services.external.model.LocationDetailApiResponse;
import com.arco.services.external.model.UserCreationApiResponse;
import com.arco.util.HttpClientException;
import com.arco.util.HttpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

@Component
public class ArcoService {

	@Value("${arco.services.header}")
	private String token;

	@Value("${arco.services.url}")
	private String baseUrl;
	
	@Autowired
	private ContentDAO contentDAO;

	public UserCreationApiResponse createUser(String emailId) throws UserExistsException,UserNotCreatedException{
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("emailId", emailId);

			Map<String, String> headers = new HashMap<>();
			headers.put("Api_Key", token);

			String response = HttpUtil.post(baseUrl + "/createuser", jsonObject.toString(), headers);
			System.out.println(response);
			
			Gson gson = new Gson();
			ArcoApiResponse apiResponse = gson.fromJson(response, ArcoApiResponse.class);
			
			if(response != null && !StringUtils.isEmpty(apiResponse.getModel())) {
				UserCreationApiResponse creationApiResponse = gson.fromJson(apiResponse.getModel().toString(), UserCreationApiResponse.class);
				return creationApiResponse;
			}else {
				throw new UserNotCreatedException();
			}
		} catch (HttpClientException|JsonSyntaxException e) {
			e.printStackTrace();
			throw new UserNotCreatedException();

		} catch (JSONException e) {
			e.printStackTrace();
			throw new UserNotCreatedException();
		}

	}

	public List<AppraisalRequest> getPendingAppraisalsForUser(ArcoUser arcoUser) throws RequestFetchingException {
		try {

			Map<String, String> headers = new HashMap<>();
			headers.put("Api_Key", token);

			String response = HttpUtil.get(baseUrl + "/getrequestsforuser/"+arcoUser.getUserCode(), headers,null);
			
			ObjectMapper mapper = new ObjectMapper(); 
			AppraisalListingApiResponse apiResponse = mapper.readValue(response.getBytes("UTF-8"), AppraisalListingApiResponse.class);
			
			if(apiResponse != null && !StringUtils.isEmpty(apiResponse.getModel())) {
			    List<AppraisalRequest> creationApiResponse = apiResponse.getModel().get("requests");
				return creationApiResponse;
			}else {
				throw new RequestFetchingException();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RequestFetchingException();

		}
	}

	public List<LocationFieldSection> getLocationFieldSections(String locationCode) throws RequestFetchingException {
		try {

			Map<String, String> headers = new HashMap<>();
			headers.put("Api_Key", token);

			String response = HttpUtil.get(baseUrl + "/getlocatondetails/"+locationCode, headers,null);
			
			ObjectMapper mapper = new ObjectMapper(); 
			LocationDetailApiResponse apiResponse = mapper.readValue(response.getBytes("UTF-8"), LocationDetailApiResponse.class);
			
			if(apiResponse != null && !StringUtils.isEmpty(apiResponse.getModel())) {
			    List<LocationFieldSection> locationDetailResponse = apiResponse.getModel().get("sections");
				return locationDetailResponse;
			}else {
				throw new RequestFetchingException();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RequestFetchingException();

		}
	}

	public List<PredefinedValue> getLocationPredefinedFieldValues(String fieldCode) throws RequestFetchingException {
		try {

			Map<String, String> headers = new HashMap<>();
			headers.put("Api_Key", token);

			String response = HttpUtil.get(baseUrl + "/getlistvalues/"+fieldCode, headers,null);
			
			ObjectMapper mapper = new ObjectMapper(); 
			FieldValueListApiResponse apiResponse = mapper.readValue(response.getBytes("UTF-8"), FieldValueListApiResponse.class);
			
			if(apiResponse != null && !StringUtils.isEmpty(apiResponse.getModel())) {
			    List<PredefinedValue> predefinedValues = apiResponse.getModel().get("values");
			    System.out.println(predefinedValues);
				return predefinedValues;
			}else {
				throw new RequestFetchingException();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RequestFetchingException();

		}
	}

	public void saveLocationDetails(String requestCode, String locationCode, LocationFieldSection[] sections) throws Exception {
		try {
			Map<String, String> headers = new HashMap<>();
			headers.put("Api_Key", token);
			
			JSONObject request = new JSONObject();
			request.put("requestCode", requestCode);
			request.put("locationCode", locationCode);
			
			JSONArray values = new JSONArray(); 
			
			for (LocationFieldSection locationFieldSection : sections) {
				List<LocationField> fields = locationFieldSection.getFieldValues();
				
				for (LocationField locationField : fields) {
					JSONObject value = new JSONObject();
					value.put("sectionCode", locationFieldSection.getSectionCode());
					value.put("fieldCode", locationField.getFieldCode());
					value.put("fieldValue", locationField.getFieldValue());
					values.put(value);
				}
			}
			
			request.put("fieldValues", values);

			String response = HttpUtil.post(baseUrl + "/saveLocationDetails", request.toString(), headers);
			System.out.println(response);
			
			Gson gson = new Gson(); 
			ArcoApiResponse apiResponse = gson.fromJson(response, ArcoApiResponse.class);
			
			if(apiResponse != null && !apiResponse.getHasError()) {
				
			}else {
				throw new Exception();
			}
		} catch (HttpClientException|JsonSyntaxException e) {
			e.printStackTrace();
			throw new Exception();

		} catch (JSONException e) {
			e.printStackTrace();
			throw new Exception();
		}
		
	}

	public void uploadLocationPicture(String locationCode, String encodedImage, String imageType) throws Exception {
		try {
			Map<String, String> headers = new HashMap<>();
			headers.put("Api_Key", token);
			
			JSONObject request = new JSONObject();
			request.put("imageType", imageType);
			request.put("locationCode", locationCode);
			request.put("file", encodedImage);
			
			String response = HttpUtil.post(baseUrl + "/savelocationimage", request.toString(), headers);
			System.out.println(response);
			
			Gson gson = new Gson(); 
			ArcoApiResponse apiResponse = gson.fromJson(response, ArcoApiResponse.class);
			
			if(apiResponse != null && !apiResponse.getHasError()) {
				
			}else {
				throw new Exception();
			}
		} catch (HttpClientException|JsonSyntaxException e) {
			e.printStackTrace();
			throw new Exception();

		} catch (JSONException e) {
			e.printStackTrace();
			throw new Exception();
		}
		
	}

	public void sendRegistrationEmail(ArcoUser arcoUser, String link)  throws Exception {
		try {
			Map<String, String> headers = new HashMap<>();
			headers.put("Api_Key", token);
			
			JSONObject request = new JSONObject();
			request.put("userName", arcoUser.getUsername());
			request.put("firstName", arcoUser.getFirstName());
			request.put("lastName", arcoUser.getLastName());
			request.put("email", arcoUser.getEmailId());
			request.put("activationUrl", link);
			
			String response = HttpUtil.post(baseUrl + "/sendregistrationemail", request.toString(), headers);
			System.out.println(response);
			
			Gson gson = new Gson(); 
			ArcoApiResponse apiResponse = gson.fromJson(response, ArcoApiResponse.class);
			
			if(apiResponse != null && !apiResponse.getHasError()) {
				
			}else {
				throw new Exception();
			}
		} catch (HttpClientException|JsonSyntaxException e) {
			e.printStackTrace();
			throw new Exception();

		} catch (JSONException e) {
			e.printStackTrace();
			throw new Exception();
		}
		
	}

	public void sendRegistrationSuccess(ArcoUser arcoUser)  throws Exception {
		try {
			Map<String, String> headers = new HashMap<>();
			headers.put("Api_Key", token);
			
			JSONObject request = new JSONObject();
			request.put("userName", arcoUser.getUsername());
			request.put("firstName", arcoUser.getFirstName());
			request.put("lastName", arcoUser.getLastName());
			request.put("email", arcoUser.getEmailId());
			request.put("activationUrl", "");
			
			String response = HttpUtil.post(baseUrl + "/sendregistrationsuccess", request.toString(), headers);
			System.out.println(response);
			
			Gson gson = new Gson(); 
			ArcoApiResponse apiResponse = gson.fromJson(response, ArcoApiResponse.class);
			
			if(apiResponse != null && !apiResponse.getHasError()) {
				
			}else {
				throw new Exception();
			}
		} catch (HttpClientException|JsonSyntaxException e) {
			e.printStackTrace();
			throw new Exception();

		} catch (JSONException e) {
			e.printStackTrace();
			throw new Exception();
		}
		
	}

	public void saveRequestLocation(String requestCode, String latitude, String longitude) throws Exception {
		try {
			Map<String, String> headers = new HashMap<>();
			headers.put("Api_Key", token);
			
			JSONObject request = new JSONObject();
			request.put("requestCode", requestCode);
			request.put("latitudeCoordinate", Double.parseDouble(latitude));
			request.put("longitudeCoordinate", Double.parseDouble(longitude));
			
			String response = HttpUtil.post(baseUrl + "/updatecoordinates", request.toString(), headers);
			System.out.println(response);
			
			Gson gson = new Gson(); 
			ArcoApiResponse apiResponse = gson.fromJson(response, ArcoApiResponse.class);
			
			if(apiResponse != null && !apiResponse.getHasError()) {
				
			}else {
				throw new Exception();
			}
		} catch (HttpClientException|JsonSyntaxException e) {
			e.printStackTrace();
			throw new Exception();

		} catch (JSONException e) {
			e.printStackTrace();
			throw new Exception();
		
	   }

	}
}
