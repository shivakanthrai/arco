package com.arco.services.external.model;

import java.util.List;
import java.util.Map;

import com.arco.model.LocationFieldSection;

public class LocationDetailApiResponse {
	private Boolean hasError;
	private Integer responseCode;
	private Map<String, List<LocationFieldSection>> model;

	public Boolean getHasError() {
		return hasError;
	}

	public void setHasError(Boolean hasError) {
		this.hasError = hasError;
	}

	public Integer getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}

	public Map<String, List<LocationFieldSection>> getModel() {
		return model;
	}

	public void setModel(Map<String, List<LocationFieldSection>> model) {
		this.model = model;
	}

}
