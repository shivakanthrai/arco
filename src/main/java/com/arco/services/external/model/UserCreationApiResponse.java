package com.arco.services.external.model;

public class UserCreationApiResponse {
	
	String userCode;

	public String getUserCode() {
		if(userCode.contains(".")) {
			userCode = String.format ("%.0f", Double.valueOf(userCode));
		}
		return userCode;
	}

	public void setUserCode(String userCode) {
		if(userCode.contains(".")) {
			userCode = String.format ("%.0f", userCode);
		}
		
		this.userCode = userCode;
	}

}
