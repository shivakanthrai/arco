package com.arco.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.arco.bo.user.UserBO;
import com.arco.model.RequestResponse;
import com.arco.services.external.ArcoService;

@Controller
public class EmailVerificationService {

	@Autowired
	private UserBO userBo;
	
	@GetMapping("/verify")
	public String verifiyEmail(@RequestParam(name="random1") String securityRandomId,
			@RequestParam(name="random2") String emailRandomId) {
		RequestResponse response = new RequestResponse();
		try {
			
			boolean isVerified = userBo.verifyEmail(securityRandomId,emailRandomId);
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setHasError(true);
			response.setMessage("User could not be added");
		}

		return "verification";
	}
}
