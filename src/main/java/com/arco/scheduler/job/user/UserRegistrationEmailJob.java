package com.arco.scheduler.job.user;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.arco.bo.content.ContentBO;
import com.arco.bo.user.UserBO;
import com.arco.exception.ArcoBusinessException;
import com.arco.model.ArcoUser;
import com.arco.model.EmailVerificationStatus;
import com.arco.services.external.ArcoService;

@Component
@DisallowConcurrentExecution
public class UserRegistrationEmailJob implements Job {

	@Autowired
	private ContentBO contentBO;

	@Autowired
	private UserBO userBO;

	@Value("${arco.mail.to_email_id}")
	private String toEmailId;

	@Value("${arco.mail.from_email_id}")
	private String fromEmailId;

	@Value("${arco.mail.password}")
	private String password;

	@Value("${arco.mail.smtp_host}")
	private String smtpMailHost;

	@Value("${arco.mail.smtp_port}")
	private String smtpPort;

	@Value("${arco.mail.email_subject}")
	private String subject;

	@Value("${arco.api.url}")
	private String apiUrl;

	@Value("${arco.mail.message_body}")
	private String messageBody;

	@Autowired
	private ArcoService arcoService;

	Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {

		ArcoUser arcoUser = userBO.getUserForSendingEmail();

		if (arcoUser != null) {
			try {
				EmailVerificationStatus verificationStatus = userBO.getEmailVerificationEntry(arcoUser.getId());

				if (verificationStatus != null) {
					String link = apiUrl + "/verify?random1=" + verificationStatus.getSecurityRandomId()
					+ "&random2=" + verificationStatus.getEmailRandomId();
					
					arcoService.sendRegistrationEmail(arcoUser,link);
					arcoUser.setEmailSentStatus("SENT");
					
				}
			} catch (Exception e) {
				arcoUser.setEmailSentStatus("FAILED");
				e.printStackTrace();
			}finally {
				try {
					userBO.updateUser(arcoUser);
				} catch (ArcoBusinessException e) {
					e.printStackTrace();
				}
			}
	}
	

//	@Override
//	public void execute(JobExecutionContext context) throws JobExecutionException {
//
//		ArcoUser arcoUser = userBO.getUserForSendingEmail();
//
//		if (arcoUser != null) {
//			try {
//				EmailVerificationStatus verificationStatus = userBO.getEmailVerificationEntry(arcoUser.getId());
//
//				if (verificationStatus != null) {
//					Properties props = new Properties();
//					props.setProperty("mail.transport.protocol", "smtp");
//					props.setProperty("mail.host", smtpMailHost);
//					props.put("mail.smtp.auth", "true");
//					props.put("mail.smtp.port", smtpPort);
//					props.put("mail.debug", "true");
//
//					Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
//						protected PasswordAuthentication getPasswordAuthentication() {
//							return new PasswordAuthentication(fromEmailId, password);
//						}
//					});
//
//					session.setDebug(true);
//
//					try {
//						Transport transport = session.getTransport();
//						InternetAddress addressFrom = new InternetAddress(fromEmailId);
//
//						MimeMessage message = new MimeMessage(session);
//
//						// 5) create Multipart object and add MimeBodyPart objects to this object
//						Multipart multipart = new MimeMultipart();
//
//						// 3) create MimeBodyPart object and set your message text
//						BodyPart messageBodyPart1 = new MimeBodyPart();
//						String link = apiUrl + "/verify?random1=" + verificationStatus.getSecurityRandomId()
//								+ "&random2=" + verificationStatus.getEmailRandomId();
//						messageBody = "<a href=\"" + link + "\">"
//								+ new String(messageBody.getBytes("ISO-8859-1"), "UTF-8") + "</a>";
//						messageBodyPart1.setContent(messageBody, "text/html");
//						multipart.addBodyPart(messageBodyPart1);
//
//						// 4) create new MimeBodyPart object and set DataHandler object to this object
//						ArcoContent resume = contentBO.getContentById(arcoUser.getResume());
//
//						if (resume != null) {
//							MimeBodyPart messageBodyPart2 = new MimeBodyPart();
//							DataSource dataSource = new ByteArrayDataSource(resume.getData(), "application/pdf");
//							messageBodyPart2.setDataHandler(new DataHandler(dataSource));
//							messageBodyPart2.setFileName(resume.getFilename());
//							multipart.addBodyPart(messageBodyPart2);
//						}
//
//						// 4) create new MimeBodyPart object and set DataHandler object to this object
//						ArcoContent accreditation = contentBO.getContentById(arcoUser.getAccreditation());
//
//						if (accreditation != null) {
//							MimeBodyPart messageBodyPart3 = new MimeBodyPart();
//							DataSource dataSource = new ByteArrayDataSource(accreditation.getData(), "application/pdf");
//							messageBodyPart3.setDataHandler(new DataHandler(dataSource));
//							messageBodyPart3.setFileName(accreditation.getFilename());
//							multipart.addBodyPart(messageBodyPart3);
//						}
//
//						// 6) set the multiplart object to the message object
//						message.setContent(multipart);
//						message.setSender(addressFrom);
//						message.setSubject(new String(subject.getBytes("ISO-8859-1"), "UTF-8"));
//						message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmailId));
//						message.addRecipient(Message.RecipientType.CC, new InternetAddress(arcoUser.getEmailId()));
//
//						transport.connect();
//						Transport.send(message);
//						transport.close();
//						arcoUser.setEmailSentStatus("SENT");
//					} catch (Exception e) {
//						arcoUser.setEmailSentStatus("FAILED");
//						e.printStackTrace();
//					} finally {
//						try {
//							userBO.updateUser(arcoUser);
//						} catch (ArcoBusinessException e) {
//							e.printStackTrace();
//						}
//					}
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
	}

}
