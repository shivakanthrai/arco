package com.arco.model;

import java.util.List;

public class AppraisalRequest {
	
	private String requestCode;
	private String requestDescription;
	private String address;
	private String latitude;
	private String longitude;
	private String status;
	private List<Location> locations;
	
	public String getRequestCode() {
		return requestCode;
	}
	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}
	public String getRequestDescription() {
		return requestDescription;
	}
	public void setRequestDescription(String requestDescription) {
		this.requestDescription = requestDescription;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Location> getLocations() {
		return locations;
	}
	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}
	
	@Override
	public String toString() {
		return "AppraisalRequest [requestCode=" + requestCode + ", requestDescription=" + requestDescription
				+ ", address=" + address + ", latitude=" + latitude + ", longitude=" + longitude + ", status=" + status
				+ ", locations=" + locations + "]";
	}
	
	
	
}
