package com.arco.model;

import java.util.List;

public class LocationFieldSection {
	
	private String sectionCode;
	
	private String sectionName;
	
	private List<LocationField> fieldValues;
	
	private boolean hasOutdoorFields;
	
	private boolean hasIndoorFields;

	public String getSectionCode() {
		return sectionCode;
	}

	public void setSectionCode(String sectionCode) {
		this.sectionCode = sectionCode;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public List<LocationField> getFieldValues() {
		return fieldValues;
	}

	public void setFieldValues(List<LocationField> fieldValues) {
		this.fieldValues = fieldValues;
	}

	public boolean isHasOutdoorFields() {
		return hasOutdoorFields;
	}

	public void setHasOutdoorFields(boolean hasOutdoorFields) {
		this.hasOutdoorFields = hasOutdoorFields;
	}

	public boolean isHasIndoorFields() {
		return hasIndoorFields;
	}

	public void setHasIndoorFields(boolean hasIndoorFields) {
		this.hasIndoorFields = hasIndoorFields;
	}
	


}
