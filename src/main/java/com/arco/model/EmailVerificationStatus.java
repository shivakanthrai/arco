package com.arco.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "EmailVerificationStatus")
public class EmailVerificationStatus {
	
	@NotNull
	@Id
	private Long userId;
	
	@NotNull
	@Column(length=500)
	private String emailRandomId;
	
	@NotNull
	@Column(length=500)
	private String securityRandomId;
	
	@NotNull
	@Column(length=500)
	private String verificationStatus;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getEmailRandomId() {
		return emailRandomId;
	}

	public void setEmailRandomId(String emailRandomId) {
		this.emailRandomId = emailRandomId;
	}

	public String getSecurityRandomId() {
		return securityRandomId;
	}

	public void setSecurityRandomId(String securityRandomId) {
		this.securityRandomId = securityRandomId;
	}

	public String getVerificationStatus() {
		return verificationStatus;
	}

	public void setVerificationStatus(String verificationStatus) {
		this.verificationStatus = verificationStatus;
	}
	
	
	

}
