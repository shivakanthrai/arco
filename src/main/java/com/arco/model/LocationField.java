package com.arco.model;

import javax.persistence.Entity;

public class LocationField {
	
	public static final String FIELD_TYPE_DATE = "date";

	private String fieldCode;
	private String fieldName;
	private String fieldDataType;
	private String fieldType;
	private String displayText;
	private Object fieldValue;
	private boolean fieldEditable;

	public String getFieldCode() {
		return fieldCode;
	}

	public void setFieldCode(String fieldCode) {
		this.fieldCode = fieldCode;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldDataType() {
		return fieldDataType;
	}

	public void setFieldDataType(String fieldDataType) {
		this.fieldDataType = fieldDataType;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getDisplayText() {
		return displayText;
	}

	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}

	public Object getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(Object fieldValue) {
		this.fieldValue = fieldValue;
	}
	
	public boolean getFieldEditable() {
		return fieldEditable;
	}

	public void setFieldEditable(boolean fieldEditable) {
		this.fieldEditable = fieldEditable;
	}

}
