package com.arco.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Location {

	private String locationCode;
	private Integer locationType;
	private String locationName;
	private String address;
	private String latitude;
	private String longitude;

	@JsonProperty(value = "locationCode")
	public String getLocationCode() {
		return locationCode;
	}

	@JsonProperty(value = "LocationCode")
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	@JsonProperty(value = "locationType")
	public Integer getLocationType() {
		return locationType;
	}

	@JsonProperty(value = "LocationType")
	public void setLocationType(Integer locationType) {
		this.locationType = locationType;
	}

	@JsonProperty(value = "locationName")
	public String getLocationName() {
		return locationName;
	}

	@JsonProperty(value = "LocationName")
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	@JsonProperty(value = "address")
	public String getAddress() {
		return address;
	}

	@JsonProperty(value = "Adress")
	public void setAddress(String address) {
		this.address = address;
	}

	@JsonProperty(value = "latitude")
	public String getLatitude() {
		return latitude;
	}

	@JsonProperty(value = "Latitude")
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@JsonProperty(value = "longitude")
	public String getLongitude() {
		return longitude;
	}

	@JsonProperty(value = "Longitude")
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "Location [locationCode=" + locationCode + ", locationType=" + locationType + ", locationName="
				+ locationName + ", address=" + address + ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}
}
